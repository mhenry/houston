import os
import time
import houston as h

my_radio = h.Radio(os.environ["LUNA_API_ID"], os.environ["LUNA_API_KEY"])

vm_id = "0f0ec975-ff6a-4163-9ea5-dcfcf5c29f78"

r = my_radio.request("vm", "unshelve", {"vm_id": vm_id})
time.sleep(30)
r = my_radio.request("vm", "start", {"vm_id": vm_id})
print(r.json())
