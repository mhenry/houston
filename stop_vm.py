import os
import time
import datetime
import houston as h


def wait_for_vm(vm_id, radio):

    print("checking status")
    r = radio.request("vm", "info", {'vm_id': vm_id})
    status = r.json()['info']['task_state']
    print(f"VM is {status}")

    while status != "" or status =="shutoff":
        print("Waiting for the VM to be ready")
        time.sleep(30)
        r = radio.request("vm", "info", {'vm_id': vm_id})
        status = r.json()['info']['status_raw']
        print(f"VM is {status}")

    print("VM ready")


def make_snap(vm_id, radio, snap_name):

    r = radio.request("vm", "snapshot", {'vm_id': vm_id, 'name': snap_name})
    print(r.json())


def shut_down(vm_id, radio):

    r = radio.request("vm", "stop", {'vm_id': vm_id})
    print(r.json())


def shelve_vm(vm_id, radio):

    r = radio.request("vm", "shelve", {'vm_id': vm_id})
    print(r.json())


vm_id = "0f0ec975-ff6a-4163-9ea5-dcfcf5c29f78"
snap_name = datetime.datetime.now().isoformat()
my_radio = h.Radio(os.environ["LUNA_API_ID"], os.environ["LUNA_API_KEY"])

wait_for_vm(vm_id, my_radio)
make_snap(vm_id, my_radio, snap_name)
wait_for_vm(vm_id, my_radio)
shut_down(vm_id, my_radio)
wait_for_vm(vm_id, my_radio)
shelve_vm(vm_id, my_radio)
