import os
import time
import hmac
import hashlib
import requests
import json


class Radio():

    def __init__(self, API_ID, API_KEY):
        if len(API_ID) != 16:
            raise InvalidArgumentException('Supplied api_id incorrect length,\
                                           must be 16')
        if len(API_KEY) != 128:
            raise InvalidArgumentException('Supplied api_key incorrect length,\
                                           must be 128')

        self.LUNA_API_ID = API_ID
        self.LUNA_API_KEY = API_KEY
        self.LUNA_API_KEY_PARTIAL = self.LUNA_API_KEY[:64]
        self.api_info = {}
        self.api_info['api_id'] = self.LUNA_API_ID
        self.api_info['api_partialkey'] = self.LUNA_API_KEY_PARTIAL

    def request(self, catagory, action, params={}):

        self.catagory = catagory
        self.action = action
        self.raw_request_payload = {**dict(params), **self.api_info}
        self.request_payload = json.dumps(self.raw_request_payload)

        self.LUNA_URL_SLUG = f"{self.catagory}/{self.action}/"
        self.LUNA_URL_ENDPOINT = f"https://dynamic.lunanode.com/api/{self.LUNA_URL_SLUG}"

        self.nonce = str(int(time.time()))
        self.hasher = hmac.new(bytes(self.LUNA_API_KEY.encode("utf-8")),
                               bytes(f"{self.LUNA_URL_SLUG}|{self.request_payload}|{self.nonce}".encode("utf-8")),
                               hashlib.sha512)

        self.signature = self.hasher.hexdigest()
        self.r = requests.post(self.LUNA_URL_ENDPOINT,
                               {'nonce': self.nonce,
                                'req': self.request_payload,
                                'signature': self.signature})
        if self.r.json()['success'] == "yes":
            return self.r
        else:
            print(self.r.json())
            raise APIException("API called failed")


class InvalidArgumentException(Exception):
    pass


class APIException(Exception):
    pass


if __name__ == "__main__":
    vm_id = "0f0ec975-ff6a-4163-9ea5-dcfcf5c29f78"

    my_radio = Radio(os.environ["LUNA_API_ID"], os.environ["LUNA_API_KEY"])
    r = my_radio.request("vm", "info", {'vm_id': vm_id})
    print(r.json())
